;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;==========================================================

[colors]
background = ${xrdb:background}
background-alt = #444
foreground = ${xrdb:foreground}
urgent = ${xrdb:color1}
foreground-alt = #555
primary = #ffb52a
secondary = #e60053
alert = #bd2c40

[all]
padding = 2 

[bar/my-bar]
width = 100%
height = 27
radius = 6.0
fixed-center = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #f00

border-size = 4
border-color = #00000000

padding-left = 1
padding-right = 2

module-margin-left = 1
module-margin-right = 2

font-0 = RobotoMono:pixelsize=9;1
font-1 = unifont:fontformat=truetype:size=8:antialias=false;0
font-2 = FontAwesome:pixelsize=11;1

modules-left = powermenu
modules-center = i3
modules-right = pulseaudio battery  date time

tray-position = right
tray-padding = 2

cursor-click = pointer
cursor-scroll = ns-resize

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

label-mode-padding = 2
label-mode-foreground = #000
label-mode-background = ${colors.primary}

label-focused = %icon%
label-focused-background = ${colors.foreground-alt}
label-focused-underline = ${colors.foreground}
label-focused-padding = ${all.padding}

label-unfocused = %icon%
label-unfocused-padding = ${all.padding}

label-visible = %icon%
label-visible-background = ${self.label-focused-background}
label-visible-padding = ${all.padding}

label-urgent = %icon%
label-urgent-background = ${colors.urgent}
label-urgent-foreground = ${colors.foreground}
label-urgent-padding = ${all.padding}

ws-icon-0 = 1;
ws-icon-1 = 2;
ws-icon-2 = 3;
ws-icon-3 = 4;
ws-icon-4 = 5;5
ws-icon-5 = 6;6
ws-icon-6 = 7;7
ws-icon-7 = 8;8
ws-icon-8 = 9;9
ws-icon-9 = 10;0
ws-icon-default = x;

[module/date]
type = internal/date
interval = 1
date = "%d %b %Y"
label =  %date%

[module/time]
type = internal/date
interval = 1
time = "%H:%M:%S"
label =  %time%

[module/pulseaudio]
type = internal/pulseaudio
format-volume = <label-volume>

label-volume =  %percentage:2%%
label-volume-foreground = ${colors.foreground}

label-muted = muted
label-muted-foreground = ${colors.foreground}

[module/battery]
type = internal/battery
battery = BAT0
adapter = ADP0
full-at = 100

format-charging = <label-charging>
label-charging =  %percentage:2%%

format-discharging = <label-discharging>
label-discharging =  %percentage:2%%

format-full = <label-full>
label-full =  %percentage:2%%

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = 
label-open-foreground = ${colors.foreground}
label-close = cancel
label-close-foreground = ${colors.secondary}
label-separator = |
label-separator-foreground = ${colors.foreground-alt}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2

menu-1-0 =  back
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = systemctl reboot

menu-2-0 =  back
menu-2-0-exec = menu-open-0
menu-2-1 = power off
menu-2-1-exec = systemctl poweroff

[settings]
screenchange-reload = true

[global/wm]
margin-top = 5
margin-bottom = 5
; vim:ft=dosini
